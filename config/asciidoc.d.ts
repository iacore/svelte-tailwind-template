declare type PreprocessorReturn = Promise<{
    code: string;
    data?: Record<string, unknown>;
    map?: string;
} | undefined>;
interface Preprocessor {
    markup: (args: {
        content: string;
        filename: string;
    }) => PreprocessorReturn;
}

export function adocPlugin(): Preprocessor
