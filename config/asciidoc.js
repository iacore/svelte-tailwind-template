import ADoc from 'asciidoctor'

const asciidoctor = ADoc()

const converter = new asciidoctor.Html5Converter()


// const content = 'http://asciidoctor.org[*Asciidoctor*] ' +
//   'running on https://opalrb.com[_Opal_] ' +
//   'brings AsciiDoc to Node.js!'
// const html = asciidoctor.convert(content) // (2)
// console.log(html) // (3)

export function adocPlugin() {
    return {
        markup({
            content,
            filename,
        }) {
            if (!filename.endsWith(".adoc")) return undefined;
            const adoc = asciidoctor.load(content, {sourcemap: true})

            return {
                    // code: string,
                    code: adoc.convert(),
                    // data?: Record<string, unknown>,
                    // map?: string,
                    // map: TODO
                }
            
        }
    }
}