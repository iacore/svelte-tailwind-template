import adapter from "@sveltejs/adapter-static"
import preprocess from "svelte-preprocess"
import { mdsvex } from "mdsvex"
import { adocPlugin } from "./config/asciidoc.js"

// import { myRemarkPlugin } from "./config/remark"
// const remarkPlugins = [remarkDirective, myRemarkPlugin] // buggy
const remarkPlugins = []

/** @type {import('@sveltejs/kit').Config} */
const config = {
  extensions: [".svelte", ".svx", ".adoc"],

  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [preprocess({postcss:true}), mdsvex({ remarkPlugins }), adocPlugin()], // swap order when in trouble

  kit: {
    adapter: adapter(),

    // // Override http methods in the Todo forms
    // methodOverride: {
    // 	allowed: ['PATCH', 'DELETE']
    // },

    prerender: {
      default: true,
    },
  },
}

export default config
